# Google2Pdf

This project is my attempt on a google request to .pdf converter console command. The application is made with
the [laravel framework](https://laravel.com/) and was created during a coding challenge.

## Contents

- [Features](#features)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Usage](#usage)
- [Future work](#future-work)

## Features

| Feature                     | Description                                                                                                                                                             |
|:----------------------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Search results as .pdf file | With the laravel command `php artisan g2pdf "searchTerm" --amount=N` the user is able to parse the first N results of a google search to a .pdf file.                   |
| Error Handling              | If anything went wrong during the execution of `g2pdf` the user will be asked if an error .pdf file should be created. Nevertheless, the behavior of the app is logged. |
| Performance                 | This App uses the [Google Custom Search JSON API](https://developers.google.com/custom-search/v1/overview). So, no time-consuming html scraping is needed.              |

## Prerequisites

#### Software
- [PHP](https://www.php.net/downloads.php) 8 with the following extensions
  - curl
  - fileinfo
  - mbstring
  - openssl
- [NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm/) 8 or above
- [Composer](https://getcomposer.org/download/) 2.2.5 or above

#### Google Api 
First, a programmable search engine needs to be created [here](https://programmablesearchengine.google.com/about/). During the creation you will get asked for a search URL. Insert a URL of your choice. After that, you need to enable the engine to search the entire web ( Basics > Search the entire web ). Be aware that your engine has a search cap of 10.000 requests per day.

Second, a Google API token needs to be generated [here](https://developers.google.com/custom-search/v1/introduction)

## Installation

Clone this repo to a directory of your choice
```shell
git clone git@gitlab.com:xStroke/google-to-pdf.git path/to/dir
```
Install the projects dependencies
```shell
composer i
npm i
```
Copy and rename the `.env.example` file to `.env`
```shell
cd path/to/project
cp .env.example .env
```
Generate the applications key
```shell
php artisan key:generate
```
Insert your Google API token and custom search engine id. If you wish you can set the following optional config values as well
```
// required config values

GOOGLE_CX=                  // custom search engine ID
GOOGLE_TOKEN=               // google api token

// optional config values, need to be set manual in your .env file 

GOOGLE_DEFAULT_AMOUNT=      // default length of the result set ( default 10 )
GOOGLE_MAX_AMOUNT=          // max length of the result set ( default 20, every 10 items need their own google api request )
GOOGLE_GL=                  // geolocation of end user ( default config('app')['locale'] )
GOOGLE_LR=                  // restricts the search to documents written in a particular language ( default '' )
GOOGLE_HL=                  // sets the user interface language ( default config('app')['locale'] )
GOOGLE_URL_PREFIX=          // defines the google interface url ( default https://www.googleapis.com/customsearch/v1 )

PDF_DEFAULT_PATH=           // default storage path of the .pdf files ( default storage_path('app\\pdf') )
PDF_MAX_SEARCH_LENGTH=      // max amount of chars that will represent the search term in the files header ( default 30 )

```

## Usage

The command can be run via `php artisan g2pdf <searchTerm> [options]`.
The `searchTerm` can contain any instructions an online Google search could handle. 

```shell
php artisan g2pdf "hello world" --amount=15 --offset=42 --path="path/to/my/pdf/file"
```

The following options are available: 

| Option    | Description                                                    |
|:----------|:---------------------------------------------------------------|
| -h --help | Explains the usage of g2pdf and shows all available parameters |
| --amount= | Length of the result set                                       |
| --offset= | Result sets offset ( default 0 )                               |
| --path=   | Absolute output path                                           |

#### Example .pdf files
Example .pdf files can be streamed by starting laravels dev server and visiting ```http://127.0.0.1:8000```
```shell
php artisan serve
```

## Future work

- All search requests and generated files could be stored in the database
- A web interface could enable online Google to .pdf creation 
