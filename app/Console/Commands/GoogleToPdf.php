<?php

namespace App\Console\Commands;

use App\Models\GooglePDF;
use App\Models\GoogleRequest;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class GoogleToPdf extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'g2pdf ' .
        '{search : Term that will be searched by the google engine} ' .
        '{--amount= : Amount of search results} ' .
        '{--offset= : Result sets offset} ' .
        '{--path= : Absolute output path}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Transforms google searches to .pdf files';

    /**
     * Max amount of search results (every 10 results need their own request)
     *
     * @var numeric
     */
    private $maxAmount;

    /**
     * Default amount of search results
     *
     * @var numeric
     */
    private $defaultAmount;

    /**
     * Default path that will be used to store files
     *
     * @var string
     */
    private $defaultPath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // set defaults
        $this->maxAmount = config('services')['googleApi']['maxAmount'];
        $this->defaultAmount = config('services')['googleApi']['amount'];
        $this->defaultPath = config('services')['domPdf']['path'];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $search = $this->argument('search');
        $offset = $this->option('offset') ?? 0;

        // validate amount option or set default
        if ($this->option('amount') && intval($this->option('amount')) <= $this->maxAmount) {

            $amount = $this->option('amount');
        } else {

            if ($this->option('amount') !== null) { // invalid amount option

                $this->warn('Invalid "amount" option. Max value is ' . $this->maxAmount . '. Amount will be set to ' . $this->maxAmount . '!');

                if ($this->confirm('Continue?')) {

                    $amount = $this->maxAmount;
                } else {

                    return 0;
                }
            } else {

                $amount = $this->defaultAmount;
            }
        }

        // validate path
        if ($this->option('path')) {

            if (!File::isDirectory($this->option('path'))) {

                $this->warn('Invalid "path" option. Path does not exist or is not a Directory. ' . $this->option('path'));
                $this->warn('Default path will be used to store the file. ' . $this->defaultPath);

                if ($this->confirm('Continue?')) {

                    $path = $this->defaultPath;
                } else {

                    return 0;
                }
            } else {

                $path = $this->option('path');
            }
        } else {

            // create default path if it does not exist
            if (!File::isDirectory($this->defaultPath)) {

                Log::channel('storage')->info('Default dir is not present. The following directory will be created: ' . $this->defaultPath);
                File::makeDirectory($this->defaultPath, 0777, true, true);
            }

            $path = $this->defaultPath;
        }

        $request = new GoogleRequest($search, $amount, $offset + 1);

        try {

            $searchJSON = $request->send();
        } catch (GuzzleException $e) { // error which cannot be handled by GoogleRequest

            $this->error($e->getCode() . ': Request failed! ' . $e->getMessage());

            return 1;
        }

        if (!GoogleRequest::validateJSON($searchJSON)) {

            $this->error($searchJSON->error->code . ': Google request failed! ' . $searchJSON->error->message);

            if ($this->confirm('Create an error .pdf file with detailed information?', true)) {

                $pdf = new GooglePDF();
                $filePath = $path . '\\' . date('Ymd_His_') . $search . '_error.pdf';
                $pdf->generateFromError($searchJSON)->save($filePath);

                $this->info('Created an error .pdf file at ' . $filePath);
            }

            return 0;

        }

        $pdf = new GooglePDF();
        $filePath = $path . '\\' . date('Ymd_His_') . $search . '.pdf';

        $pdf->generateFromSearch($searchJSON)->save($filePath);

        Log::channel('storage')->info('Stored .pdf file: ' . $filePath);
        $this->info('Created search .pdf file at ' . $filePath);

        return 0;
    }
}
