<?php

namespace App\Http\Controllers;

use App\Models\GooglePDF;
use Illuminate\Http\Response;

class MockController extends Controller
{
    /**
     * Returns a mocked error pdf file as stream
     *
     * @return Response
     */
    public function error(): Response
    {

        $errJSON = json_decode(file_get_contents(storage_path('mock/error.json')));

        return (new GooglePDF())
            ->generateFromError($errJSON)
            ->stream('g2pdf_error_mock.pdf')
            ->header('Content-Disposition', 'inline; filename=g2pdf_error_mock.pdf');

    }

    /**
     * Returns a mocked search pdf file as stream
     *
     * @return Response
     */
    public function success(): Response
    {

        $errJSON = json_decode(file_get_contents(storage_path('mock/search.json')));

        return (new GooglePDF())
            ->generateFromSearch($errJSON)
            ->stream('g2pdf_error_mock.pdf')
            ->header('Content-Disposition', 'inline; filename=g2pdf_error_mock.pdf');

    }
}
