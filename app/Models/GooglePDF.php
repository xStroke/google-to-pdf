<?php

namespace App\Models;

use Barryvdh\DomPDF\PDF;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class GooglePDF extends Model
{
    use HasFactory;

    /**
     * Dompdf that will be created via generateFrom...()
     *
     * @var PDF pdf
     */
    private $pdf;

    /**
     * Create a new GooglePDF instance
     */
    public function __construct()
    {
        parent::__construct();

        $this->pdf = App::make('dompdf.wrapper');
    }

    /**
     * Generates a Dompdf from the given GoogleRequest search result
     *
     * @param object $searchJSON search result of a GoogleRequest
     * @return PDF
     * @see GoogleRequest
     */
    public function generateFromSearch(object $searchJSON): PDF
    {

        $parameters = [
            'search' => $searchJSON->queries->request[0]->searchTerms,
            'count' => $searchJSON->queries->request[0]->count,
            'offset' => $searchJSON->queries->request[0]->startIndex - 1
        ];

        $data = [
            'search' => $searchJSON->queries->request[0]->searchTerms,
            'totalResults' => $searchJSON->searchInformation->formattedTotalResults,
            'searchTime' => $searchJSON->searchInformation->formattedSearchTime,
            'count' => $searchJSON->queries->request[0]->count,
            'items' => $searchJSON->items,
            'parameters' => $parameters
        ];

        $this->pdf->loadView('pdf.search', $data);

        Log::channel('storage')->info('Loaded a search GooglePDF with the following data: ' . json_encode($data));

        return $this->pdf;
    }

    /**
     * Generates a Dompdf from the given GoogleRequest error object
     *
     * @param object $errorJSON error JSON of a GoogleRequest
     * @return PDF
     * @see GoogleRequest
     */
    public function generateFromError(object $errorJSON): PDF
    {
        $data = [
            'code' => $errorJSON->error->code,
            'error' => $errorJSON->error,
            'parameters' => $errorJSON->parameters,
            'url' => $errorJSON->url
        ];

        $this->pdf->loadView('pdf.error', $data);

        Log::channel('storage')->info('Loaded an error GooglePDF with the following data: ' . json_encode($data));

        return $this->pdf;
    }

}
