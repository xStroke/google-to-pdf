<?php

namespace App\Models;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class GoogleRequest extends Model
{
    use HasFactory;

    /**
     * search term
     *
     * @var string
     */
    private $search;

    /**
     * amount of results that will be requested
     *
     * @var numeric
     */
    private $amount;

    /**
     * result sets offset
     *
     * @var numeric
     */
    private $offset;

    // config values

    private $cx;
    private $gl;
    private $lr;
    private $hl;
    private $apiToken;

    /**
     * Create a new GoogleRequest instance
     *
     * @param string $search search term
     * @param numeric $amount amount of results that will be requested
     * @param numeric $offset result sets offset
     */
    public function __construct($search, $amount, $offset)
    {
        parent::__construct();

        $this->search = $search;
        $this->amount = $amount;
        $this->offset = $offset;

        $config = config('services')['googleApi'];

        $this->cx = $config['cx'];
        $this->gl = $config['gl'];
        $this->lr = $config['lr'];
        $this->hl = $config['hl'];
        $this->apiToken = $config['token'];

    }

    /**
     * Takes a request result and returns true if the result contains no errors
     *
     * @param object|null $JSON request result
     * @return bool
     */
    public static function validateJSON(object | null $JSON): bool
    {
        if (!$JSON) {
            return false;
        }

        $JSONKeys = array_keys(get_object_vars($JSON));

        return !in_array('error', $JSONKeys);
    }

    /**
     * Sends a GoogleRequest and returns its result as object
     *
     * @return object | null
     * @throws GuzzleException
     */
    public function send(): ?object
    {

        $client = new Client(['verify' => false]);

        while ($this->amount > 0) { // every 10 items need their own request

            $url = config('services')['googleApi']['urlPrefix'] . '?' .
                'q=' . $this->search . '&' .
                'num=' . min($this->amount, 10) . '&' .
                'start=' . $this->offset . '&' .
                'lr=' . $this->lr . '&' .
                'safe=active&' .
                'cx=' . $this->cx . '&' .
                'gl=' . $this->gl . '&' .
                'hl=' . $this->hl . '&' .
                'key=' . $this->apiToken . '&' .
                'alt=json';

            try {

                Log::channel('network')->info('Sending a get request to: ' . $url);
                $r = $client->get($url);

                $rJSON = json_decode($r->getBody());
                Log::channel('network')->info('Request succeeded: ' . json_encode($rJSON));

                if (!isset($result)) {

                    $result = $rJSON;
                } else {

                    $result->queries->request[0]->count += $rJSON->queries->request[0]->count;
                    $result->items = array_merge($result->items, $rJSON->items);
                }
            } catch (BadResponseException $e) {

                Log::channel('network')->error('Request failed: ' . $e->getResponse()->getBody());

                // create custom error
                return $this->getCustomError(json_decode($e->getResponse()->getBody()), $url);
            }

            $this->amount -= 10;
            $this->offset += 10;
        }

        return $result ?? null;
    }

    /**
     * Take an error object of the Google API and returns a custom error object
     *
     * @param object $errJSON error object of the Google API
     * @param string $url request url
     * @return object
     */
    private function getCustomError(object $errJSON, string $url): object
    {

        return (object)[
            "error" => $errJSON->error,
            "parameters" => (object)[
                'q' => $this->search,
                'num' => min($this->amount, 10),
                'start' => $this->offset,
                'lr' => $this->lr,
                'safe' => 'active',
                'cx' => $this->cx,
                'gl' => $this->gl,
                'hl' => $this->hl,
                'key' => $this->apiToken,
                'alt' => 'json'
            ],
            "url" => $url
        ];
    }

}
