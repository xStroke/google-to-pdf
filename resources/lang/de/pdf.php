<?php

return [

    'search' => 'Suchbegriff',
    'count' => 'Anzahl der Ergebnisse',
    'offset' => 'Versatz',

    'of' => 'von',

    'resultSummary' => 'Ihre Suche ergab :totalResults Ergebnisse in :searchTime s.',
    'searchParameters' => 'Such-Parameter',
    'searchResults' => 'Such-Ergebnisse',

    'searchURL' => 'Such-URL',
    'errorMessage' => 'Fehlermeldung',
    'searchFailedSummary' => 'Ihre Suche nach ":search" schlug fehl!'

];
