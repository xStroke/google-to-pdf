@extends('pdf.template')

@section('title')
    <span class="pdf-danger-text">{{env('APP_NAME')}}</span>
@endsection

@section('subtitle')
    {{
        __('pdf.searchFailedSummary',
            [
                'search' => strlen($parameters->q) > config('services')['domPdf']['maxSearchLength'] ?
                    substr($parameters->q,0,config('services')['domPdf']['maxSearchLength'])." ..."
                        : $parameters->q
            ]
        )
    }}
@endsection

@section('header')
    {{env('APP_NAME')}}
    -
    {{$code}}
    -
    {{date(' H:i:s - d.m.Y')}}
@endsection

@section('content')
    <div class="parameter-title" style="margin-top: 36px;">
        {{__('pdf.searchURL')}}
    </div>

    <div class="pdf-margin">
        <a href="{{$url}}" class="pdf-link">
            {{$url}}
        </a>
    </div>

    <div class="parameter-title" style="margin-top: 36px;">
        {{__('pdf.searchParameters')}}
    </div>

    <table class="parameter-wrapper">

        @foreach($parameters as $key => $value)
            <tr>
                <td>{{$key}}</td>
                <td>{{$value}}</td>
            </tr>
        @endforeach


    </table>


    <div class="parameter-title" style="margin-top: 36px;">
        {{__('pdf.errorMessage')}}
    </div>

    <div class="pdf-margin">
        <pre class="pdf-accent-text">{{json_encode($error, JSON_PRETTY_PRINT)}}</pre>
    </div>

@endsection
