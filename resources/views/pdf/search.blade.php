@extends('pdf.template')

@section('title')
    {{env('APP_NAME')}}
@endsection

@section('subtitle')
    {{__('pdf.resultSummary', ['totalResults' => $totalResults, 'searchTime' => $searchTime])}}
@endsection

@section('header')
    {{env('APP_NAME')}}
    -
    {{strlen($search) > config('services')['domPdf']['maxSearchLength'] ? substr($search,0,config('services')['domPdf']['maxSearchLength'])." ..." : $search}}
    -
    {{date(' H:i:s - d.m.Y')}}
@endsection

@section('content')

    <div class="parameter-title" style="margin-top: 36px;">
        {{__('pdf.searchParameters')}}
    </div>

    <table class="parameter-wrapper">

        @foreach($parameters as $key => $value)
            <tr>
                <td>{{__('pdf.' . $key)}}</td>
                <td>{{$value}}</td>
            </tr>
        @endforeach


    </table>



    <div class="item-headline" style="margin-top: 36px;">
        {{__('pdf.searchResults')}}
    </div>

    @foreach($items as $item)

        <div class="item-wrapper">
            <a href="{{$item->link}}" class="item-title">
                {{$item->title}}
            </a>
            <div class="item-link" >
                {{$item->link}}
            </div>
            <div class="item-snippet">
                {{$item->snippet}}
            </div>
        </div>

    @endforeach
@endsection
