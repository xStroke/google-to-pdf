<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

        <title>{{env('APP_NAME')}} - Example</title>

        <link href="{{ public_path('css/pdf.css') }}" rel="stylesheet">

        <style>
            @font-face {
                font-family: 'Nunito';
                src: url({{ storage_path('Nunito/static/Nunito-Regular.ttf') }}) format("truetype");
                font-weight: 400;
                font-style: normal;
            }

            @page {
                margin: 100px 50px;
            }

            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>

    </head>
    <body>
        {{--page count--}}
        <script type="text/php">
            if ( isset($pdf) ) {
                $x = 535;
                $y = 810;
                $text = "{PAGE_NUM} {{__('pdf.of')}} {PAGE_COUNT}";
                $font = $fontMetrics->get_font("helvetica");
                $size = 8;
                $color = array(0,0,0);
                $word_space = 0.0;  //  default
                $char_space = 0.0;  //  default
                $angle = 0.0;   //  default
                $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
            }
        </script>

        <header>
            <div>
                @yield('header')
            </div>
        </header>

        <div class="pdf-title-wrapper">

            <div class="pdf-title">
                @yield('title')
            </div>

            <div class="pdf-subtitle">
                {{date(' H:i:s - d.m.Y')}}
            </div>
        </div>

        <div class="pdf-subtitle">
            @yield('subtitle')
        </div>

        <div class="pdf-body">
            @yield('content')
        </div>

    </body>
</html>
