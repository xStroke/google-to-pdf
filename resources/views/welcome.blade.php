<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{env('APP_NAME')}}</title>

        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                margin: 0;
            }
        </style>
    </head>
    <body>
     <div class="welcome-container">

         <div class="welcome-title">
             {{env('APP_NAME')}}
         </div>
         <div class="welcome-links d-flex justify-content-center">

             <a href="https://gitlab.com/xStroke/google-to-pdf">Project's GitLab</a>
             <a href="{{route('mockSuccess')}}">Example Search File</a>
             <a href="{{route('mockError')}}">Example Error File</a>

         </div>

     </div>

    </body>
</html>
